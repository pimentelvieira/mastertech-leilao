package com.mastertech.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class LeilaoTest {

    @Test
    public void testaAdicionarNovoLance() {
        Leilao leilao = new Leilao(new ArrayList<>());
        leilao.adicionarLance(new Lance(new Usuario(1, "William"), 100));
        Assertions.assertEquals(leilao.getLances().size(), 1);
    }

    @Test
    public void testaValidarLance_LanceValido() {
        Leilao leilao = new Leilao(new ArrayList<>());
        leilao.adicionarLance(new Lance(new Usuario(1, "William"), 100));
        boolean resultadoValidacao = leilao.validarLance(new Lance(new Usuario(2, "Roberto"), 200));
        Assertions.assertTrue(resultadoValidacao);
    }

    @Test
    public void testaValidarLance_LanceInvalido() {
        Leilao leilao = new Leilao(new ArrayList<>());
        leilao.adicionarLance(new Lance(new Usuario(1, "William"), 100));
        boolean resultadoValidacao = leilao.validarLance(new Lance(new Usuario(2, "Roberto"), 50));
        Assertions.assertFalse(resultadoValidacao);
    }
}
