package com.mastertech.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class LeiloeiroTest {

    @Test
    public void testaMaiorLance() {
        Usuario usuario1 = new Usuario(1, "William");
        Usuario usuario2 = new Usuario(2, "Roberto");
        Usuario usuario3 = new Usuario(3, "Felipe");

        Lance lance1 = new Lance(usuario1, 100);
        Lance lance2 = new Lance(usuario2, 600);
        Lance lance3 = new Lance(usuario3, 300);

        Leilao leilao = new Leilao(Arrays.asList(lance1, lance2, lance3));

        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro teste", leilao);

        Assertions.assertEquals(600, leiloeiro.retornarMaiorLance().getValorDoLance());
    }
}
