package com.mastertech.leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        Lance result = null;

        for(Lance lance : this.getLeilao().getLances()) {
            if (result == null) {
                result = lance;
            } else {
                if(lance.getValorDoLance() > result.getValorDoLance()) {
                    result = lance;
                }
            }
        }

        return result;
    }
}
