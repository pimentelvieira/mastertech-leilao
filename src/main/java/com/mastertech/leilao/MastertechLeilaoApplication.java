package com.mastertech.leilao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechLeilaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechLeilaoApplication.class, args);
	}

}
