package com.mastertech.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void adicionarLance(Lance lance) {
        if(validarLance(lance)) {
            this.lances.add(lance);
        }
    }

    public boolean validarLance(Lance lance) {
        return this.lances.isEmpty() || lance.getValorDoLance() > this.lances.get(this.lances.size() - 1).getValorDoLance();
    }
}
